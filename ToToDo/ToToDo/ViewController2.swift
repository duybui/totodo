//
//  ViewController2.swift
//  ToToDo
//
//  Created by Duy Bui on 1/12/19.
//  Copyright © 2019 DuyBui. All rights reserved.
//

import UIKit

class ViewController2: UIViewController {

  @IBOutlet weak var tableView: UITableView!
  var array: [Model] = []
  var date: Int?
  
  override func viewDidLoad() {
    
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
  }
  
  
  override func viewWillAppear(_ animated: Bool) {
    print("Here")
    guard let date = date else {
      return
    }
    array = Constant.shared.list[date].model ?? []
    tableView.reloadData()
  }
  
  @IBAction func didTapOnAddButton(_ sender: Any) {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let view = storyboard.instantiateViewController(withIdentifier: "ViewController3") as! ViewController3
    view.date = date
    self.present(view, animated: true, completion: nil)
  }
  
  @IBAction func backButton(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)
  }
}

extension ViewController2: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return array.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
    cell?.textLabel?.text = array[indexPath.row].name ?? ""
    cell?.detailTextLabel?.text = array[indexPath.row].time ?? ""
    return cell!
  }
}

extension ViewController2: UITableViewDelegate {
  
}
