//
//  Model.swift
//  ToToDo
//
//  Created by Duy Bui on 1/12/19.
//  Copyright © 2019 DuyBui. All rights reserved.
//

import Foundation
struct Model {
  
  let name: String?
  let time: String?
}

struct Model1 {
  
  let date: Int?
  var model: [Model]?
}

class Constant {
  static let shared = Constant()
  // enum
   let monday = Model1(date: 0, model: [Model(name: "SC", time: "12h"),
                                        Model(name: "SB", time: "13h"),
                                        Model(name: "SC", time: "14h"),
                                        Model(name: "LS", time: "20h"),
                                        Model(name: "SC", time: "22h")])
  
  let tuesday = Model1(date: 1, model: [Model(name: "SC", time: "12h"),
                                       Model(name: "SB", time: "13h"),
                                       Model(name: "SC", time: "14h")])
  
  let wednesday = Model1(date: 2, model: [Model(name: "SC", time: "12h")])
  
  let thursday = Model1(date: 3, model: [Model(name: "SC", time: "12h"),
                                       Model(name: "SB", time: "13h"),
                                       Model(name: "SC", time: "14h"),
                                       Model(name: "LS", time: "20h"),
                                       Model(name: "SC", time: "22h"),
                                       Model(name: "LS", time: "20h"),
                                       Model(name: "SC", time: "22h")])
  
  let friday = Model1(date: 4, model: [Model(name: "SC", time: "12h"),
                                       Model(name: "SB", time: "13h"),
                                       Model(name: "SC", time: "14h"),
                                       Model(name: "LS", time: "20h")])
  
  let saturday = Model1(date: 5, model: [Model(name: "SC", time: "12h"),
                                       Model(name: "SB", time: "13h"),
                                       Model(name: "SC", time: "14h"),
                                       Model(name: "LS", time: "20h"),
                                       Model(name: "SC", time: "22h"),
                                       Model(name: "SB", time: "13h"),
                                       Model(name: "SC", time: "14h"),
                                       Model(name: "LS", time: "20h"),
                                       Model(name: "SC", time: "22h")])
  
  let sunday = Model1(date: 6, model: [Model(name: "SC", time: "12h"),
                                       Model(name: "SB", time: "13h")])
  
  var list: [Model1]
  private init() {
    self.list = [monday, tuesday, wednesday, thursday, friday, saturday, sunday]
  }
}
