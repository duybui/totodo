//
//  ViewController3.swift
//  ToToDo
//
//  Created by Duy Bui on 1/12/19.
//  Copyright © 2019 DuyBui. All rights reserved.
//

import UIKit

class ViewController3: UIViewController {

  @IBOutlet weak var nameTextField: UITextField!
  @IBOutlet weak var timeTextField: UITextField!
  
  var date: Int?
  
  override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
  @IBAction func didTapOnBackButton(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)
  }
  
  
  @IBAction func didTapOnDoneButton(_ sender: Any) {
    guard let date = date else { return }
    let model = Model(name: nameTextField.text ?? "No", time: timeTextField.text ?? "No")
    guard var modelUpdate = Constant.shared.list[date].model else { return }
    modelUpdate.append(model)
    Constant.shared.list[date].model = modelUpdate
    self.dismiss(animated: true, completion: nil)
  }
}
